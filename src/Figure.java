abstract public class Figure {
    double perimeter,area;
    abstract void Perimeter();
    abstract void Area();
    abstract void Info();
    Figure(){
        Perimeter();
        Area();
    }
}
