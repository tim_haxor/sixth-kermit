import java.util.*;

public class Main {
    public static void main(String[] args) {
        List<Figure> shapes = new ArrayList<>();
        List<Library> books = new ArrayList<>();
        Scanner sin = new Scanner(System.in);
        int d;
        do{
            System.out.println("1 - Students\n2 - Books\n3 - Shapes\n-1 - Exit");
            d = sin.nextInt();
            switch(d){
                case 1:
                    Student[] students = new Student[10];
                    for(int i =0;i<10;i++){
                        System.out.println("\n");
                        students[i] = new Student(sin.next(),sin.next());
                    }
                    System.out.println("\n\n\n\n\n"); students = sortarray(students);
                    for (Student a:students) {
                        System.out.println("\nName: "+a.name + "\nGroup: " + a.group + "\nGrade: " + a.grade);
                    }
                    System.out.println("\n\n\n\n\n");
                    for (Student a:students) {
                        boolean ggr=true;
                        for (int b:a.grades) {
                            if(b<4){
                                ggr=false;
                             break;
                            }
                        }
                        if(ggr==true) {
                            System.out.println("\nName: "+a.name + "\nGroup: " + a.group + "\nGrade: " + a.grade+"\n");
                        }else{
                            ggr=true;
                        }
                    }
                    break;
                case 2:
                    do{
                    System.out.println("\n1 - Add\n2 - Show all books\n3 - Search by author\n4 - Search by year\n5 - Search by pages amount\n6 - Remove");
                    d = sin.nextInt();
                    switch (d) {
                        case 1: books.add(new Library(sin.next(),sin.next(),sin.next(),sin.nextInt())); break;
                        case 2:
                            for (Library a: books) {
                                System.out.println("\nName: "+a.name+"\nAuthor: "+a.author+"\nYear: "+a.year+"\nPages: "+a.pages);
                            }break;
                        case 3:
                            String au = sin.next();
                            for (Library a: books) {
                                if(a.author.equals(au)){
                                    System.out.println("\nName: "+a.name+"\nAuthor: "+a.author+"\nYear: "+a.year+"\nPages: "+a.pages);
                                }
                            }
                           break;
                        case 4:
                            String year = sin.next();
                            for (Library a: books) {
                            if(a.year.equals(year)){
                                System.out.println("\nName: "+a.name+"\nAuthor: "+a.author+"\nYear: "+a.year+"\nPages: "+a.pages);
                            }
                        } break;
                        case 5:
                            int pg = sin.nextInt();
                            for (Library a: books) {
                            if(a.pages==pg){
                                System.out.println("\nName: "+a.name+"\nAuthor: "+a.author+"\nYear: "+a.year+"\nPages: "+a.pages);
                            }
                        }break;
                        case 6:
                            if(books.size()>0){
                                books.remove(sin.nextInt());
                            } break;
                        default: break;
                        }
                    }while (d!=0); break;
                case 3:
                    do {
                    System.out.println("\n1 - Add Rectangle\n2 - Add Triangle\n3 - Add Circle\n4 - Show info");
                        d = sin.nextInt();
                        switch (d) {
                            case 1:
                                System.out.println("Enter width and length");
                                shapes.add(new Rectangle(sin.nextDouble(), sin.nextDouble()));
                                break;
                            case 2:
                                System.out.println("Enter a, b, c sides");
                                shapes.add(new Triangle(sin.nextDouble(), sin.nextDouble(), sin.nextDouble()));
                                break;
                            case 3:
                                System.out.println("Enter radius");
                                shapes.add(new Circle(sin.nextDouble()));
                                break;
                            case 4:
                                for (Figure a : shapes) {
                                    a.Info();
                                } break;
                            default: break;
                        }
                    }while(d!=0);
                    break;
                    default: break;
            }
        }while (d!=-1);
    }
    public static Student[] sortarray(Student[] students){
        int n = students.length;
        for (int i = 0; i < n - 1; i++)
            for (int j = 0; j < n -i-1; j++) {
                if (students[j].grade > students[j + 1].grade) {
                    Student temp = students[j];
                    students[j] = students[j + 1];
                    students[j + 1] = temp;
                }
            }
        List<Student> temp = Arrays.asList(students);
            Collections.reverse(temp);
           students = (Student[])temp.toArray();
        return  students;
    }
}
