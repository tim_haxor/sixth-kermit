public class Triangle extends  Figure {
    double a,b,c;
    double height;
    void Perimeter(){
        perimeter = a+b+c;
    }
    void Area(){
        height = perimeter/2;
        area =Math.sqrt(height*(height-a)*(height-b)*(height-c));
        height = 2 * (area/b);
    }
    void Info(){
        System.out.println("\nTriangle_______\nHeight = "+height+"\nPerimeter = "+perimeter+"\nArea = "+area);
    }
    Triangle(double a,double b,double c){
        this.a = a;
        this.b = b;
        this.c = c;
        Perimeter();
        Area();
        Info();
    }
}
