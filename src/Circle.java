public class Circle extends Figure{
    double radius;
    void Perimeter(){
        perimeter = 2*Math.PI * radius;
    }
    void Area(){
        area = Math.PI * Math.pow(radius,2);
    }
    void Info(){
        System.out.println("\nCircle_______\nRadius = "+radius+"\nPerimeter = "+perimeter+"\nArea = "+area);
    }
    Circle(double r){
        this.radius = r;
        Perimeter();
        Area();
        Info();
    }
}
